<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace Csulok\sEngineNG;

interface EngineInterface {
    
    /**
     * 
     * @return ConfigInterface
     */
    public function config();
    
    /**
     * 
     * @return DaoInterface
     */
    public function dao();
    
    /**
     * @return DatabaseInterface
     */
    public function db();
    
    /**
     * 
     * @return HelperInterface
     */
    public function helper();
    
    /**
     * 
     * @return RouterInterface
     */
    public function router();
}